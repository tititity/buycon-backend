package senior.project.buyconbackend.util

import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers
import senior.project.buyconbackend.entity.Beacon
import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.DishRequest
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.entity.dto.BeaconDto
import senior.project.buyconbackend.entity.dto.DishDto
import senior.project.buyconbackend.entity.dto.DishRequestDto
import senior.project.buyconbackend.entity.dto.RestaurantDto

@Mapper(componentModel = "spring")
interface MapperUtil{

    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @InheritInverseConfiguration
    fun mapDishDto(dish: DishDto): Dish
    fun mapDishDto(dish: Dish): DishDto
    fun mapDishDto(dishes: List<Dish>): List<DishDto>

    fun mapRestaurantDto(restaurants: List<Restaurant>): List<RestaurantDto>
    fun mapRestaurantDto(restaurant: Restaurant?):  RestaurantDto?

    @InheritInverseConfiguration
    fun mapRestaurant(restDto: RestaurantDto): Restaurant
    fun mapRestaurant(rest: Restaurant): Restaurant


    fun mapBeaconDto(beacon: Beacon): BeaconDto

    @InheritInverseConfiguration
    fun mapBeaconDto(beaconDto: BeaconDto): Beacon
    fun mapBeaconDto(beacons: List<Beacon>): List<BeaconDto>

    @InheritInverseConfiguration
    fun mapDishRequest(dishRequestDto: DishRequestDto): DishRequest
    fun mapDishRequest(dishRequest: DishRequest): DishRequestDto
    fun mapDishRequest(dishRequests: List<DishRequest>): List<DishRequestDto>

}