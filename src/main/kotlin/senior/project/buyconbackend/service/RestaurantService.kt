package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.entity.dto.RestaurantDto

interface RestaurantService {
    fun getRestaurants(): List<Restaurant>
    fun getRestaurantByName(name: String): Restaurant?
    fun save(restaurant: RestaurantDto): Restaurant
}