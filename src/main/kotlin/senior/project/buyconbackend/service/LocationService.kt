package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Location

interface LocationService {
    fun save(location: Location): Location
}