package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.LocationDao
import senior.project.buyconbackend.entity.Location

@Service
class LocationServiceImpl: LocationService {

   @Autowired
   lateinit var locationDao: LocationDao
    override fun save(location: Location): Location {
        return locationDao.save(location)
    }

}