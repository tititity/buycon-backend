package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.CustomerDao
import senior.project.buyconbackend.dao.DishDao
import senior.project.buyconbackend.dao.DishRequestDao
import senior.project.buyconbackend.dao.RestaurantDao
import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.DishRequest
import senior.project.buyconbackend.entity.dto.DishRequestDto
import senior.project.buyconbackend.util.MapperUtil
import javax.transaction.Transactional

@Service
class DishRequestServiceImpl: DishRequestService {

    @Autowired
    lateinit var dishRequestDao: DishRequestDao

    @Autowired
    lateinit var restaurantDao: RestaurantDao

    @Autowired
    lateinit var customerDao: CustomerDao

    @Autowired
    lateinit var dishDao: DishDao

    override fun getOrders(): List<DishRequest> {
        return dishRequestDao.getOrders()
    }

    override fun getOrderByCustId(custId: Long): List<DishRequest> {
        return dishRequestDao.findByCustId(custId)
    }

    @Transactional
    override fun save(restId: Long, custId: Long, dishRequestDto: DishRequestDto): DishRequest {
        val dishRequest = MapperUtil.INSTANCE.mapDishRequest(dishRequestDto)
        var order = dishRequestDao.save(dishRequest)
        val customer = customerDao.findById(custId)
        val restaurant = restaurantDao.findById(restId)
        order.customer = customer
        order.restaurant = restaurant
        return order

    }

    override fun getOrderByRestId(restId: Long): List<DishRequest> {
        return dishRequestDao.findByRestId(restId)
    }

    override fun getOrderById(id: Long): DishRequest {
        return dishRequestDao.findById(id)
    }
}