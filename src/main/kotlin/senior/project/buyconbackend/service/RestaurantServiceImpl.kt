package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.DishDao
import senior.project.buyconbackend.dao.LocationDao
import senior.project.buyconbackend.dao.RestaurantDao
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.entity.dto.RestaurantDto
import senior.project.buyconbackend.util.MapperUtil
import javax.transaction.Transactional

@Service
class RestaurantServiceImpl: RestaurantService {


    @Autowired
    lateinit var restaurantDao: RestaurantDao

    @Autowired
    lateinit var locationDao: LocationDao

    @Autowired
    lateinit var dishDao: DishDao

    override fun getRestaurants(): List<Restaurant> {
        return restaurantDao.getRestaurants()
    }

    override fun getRestaurantByName(name: String): Restaurant? {
        return restaurantDao.getRestaurantByName(name)
    }

    @Transactional
    override fun save(rest: RestaurantDto): Restaurant {
        val rest = MapperUtil.INSTANCE.mapRestaurant(rest)
        val location = rest?.location?.let { locationDao.save(it) }
        rest?.location?.let { rest.location = location }
        return restaurantDao.save(rest)
    }


}