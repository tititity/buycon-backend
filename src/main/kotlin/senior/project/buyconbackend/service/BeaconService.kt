package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Beacon

interface BeaconService {
    fun getBeaconByRestId(restId: Long): List<Beacon>
    fun save(beacon: Beacon, restId: Long): Beacon
    fun getBeaconByRestName(restName: String): List<Beacon>
}