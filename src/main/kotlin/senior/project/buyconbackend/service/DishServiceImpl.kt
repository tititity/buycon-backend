package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.DishDao
import senior.project.buyconbackend.dao.RestaurantDao
import senior.project.buyconbackend.entity.Dish
import javax.transaction.Transactional

@Service
class DishServiceImpl : DishService {

    @Autowired
    lateinit var dishDao: DishDao

    @Autowired
    lateinit var restaurantDao: RestaurantDao

    override fun getDishByRestName(name: String): List<Dish> {
        return dishDao.getDishByRestName(name)
    }

    @Transactional
    override fun save(dish: Dish): Dish {
        val rest = dish?.restaurant?.let { restaurantDao.save(it) }
        val dish = dishDao.save(dish)
        rest?.dishes?.add(dish)
        return dish
    }

    @Transactional
    override fun save(restId: Long, dish: Dish): Dish {
        val restaurant = restaurantDao.findById(restId)
        val dish = dishDao.save(dish)
        dish.restaurant = restaurant
        restaurant?.dishes?.add(dish)
        return dish
    }

}