package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Menu

interface MenuService {

    fun getMenus(): List<Menu>
}