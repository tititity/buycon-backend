package senior.project.buyconbackend.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import senior.project.buyconbackend.dao.BeaconDao
import senior.project.buyconbackend.dao.RestaurantDao
import senior.project.buyconbackend.entity.Beacon
import javax.transaction.Transactional

@Service
class BeaconServiceImpl: BeaconService{

    @Autowired
    lateinit var beaconDao: BeaconDao

    @Autowired
    lateinit var restaurantDao: RestaurantDao

    override fun getBeaconByRestId(restId: Long): List<Beacon> {
        return beaconDao.getBeaconByRestId(restId)
    }
    @Transactional
    override fun save(beacon: Beacon, restId: Long): Beacon {
        val rest = restaurantDao.findById(restId)
        val beacon = beaconDao.save(beacon)
        rest?.beacons?.add(beacon)
        beacon.restaurant = rest
        return beacon
    }

    override fun getBeaconByRestName(restName: String): List<Beacon> {
        return beaconDao.findByRestName(restName)
    }

}