package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Dish
import senior.project.buyconbackend.entity.dto.DishDto

interface DishService {

    fun getDishByRestName(name: String): List<Dish>
    fun save(dish: Dish): Dish
    fun save(restId: Long, dish: Dish): Dish
}