package senior.project.buyconbackend.service

import senior.project.buyconbackend.entity.Customer
import senior.project.buyconbackend.entity.DishRequest
import senior.project.buyconbackend.entity.dto.DishRequestDto

interface DishRequestService {
    fun getOrders(): List<DishRequest>
    fun save(restId:Long, cutId:Long, dishRequestDto: DishRequestDto): DishRequest
    fun getOrderByCustId(custId: Long): List<DishRequest>
    fun getOrderByRestId(restId: Long): List<DishRequest>
    fun getOrderById(id: Long): DishRequest
}