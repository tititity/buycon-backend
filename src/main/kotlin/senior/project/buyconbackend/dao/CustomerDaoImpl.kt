package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Customer
import senior.project.buyconbackend.repository.CustomerRepository

@Repository
class CustomerDaoImpl: CustomerDao {

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun findById(custId: Long): Customer? {
        return customerRepository.findById(custId).orElse(null)
    }
}