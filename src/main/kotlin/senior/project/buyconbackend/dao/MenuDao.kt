package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Menu

interface MenuDao {
    fun getMenus(): List<Menu>
}