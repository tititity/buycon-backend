package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Beacon
import senior.project.buyconbackend.repository.BeaconRepository

@Repository
class BeaconDaoImpl : BeaconDao{

    @Autowired
    lateinit var beaconRepository: BeaconRepository

    override fun getBeaconByRestId(restId: Long): List<Beacon> {
        return beaconRepository.findByRestaurant_Id(restId)
    }

    override fun save(beacon: Beacon): Beacon {
        return beaconRepository.save(beacon)
    }

    override fun findByRestName(restName: String): List<Beacon> {
        return beaconRepository.findByRestaurant_Name(restName)
    }
}