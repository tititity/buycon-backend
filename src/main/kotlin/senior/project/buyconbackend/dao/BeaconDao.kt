package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Beacon


interface BeaconDao {
    fun getBeaconByRestId(restId: Long): List<Beacon>
    fun save(beacon: Beacon): Beacon
    fun findByRestName(restName: String): List<Beacon>
}