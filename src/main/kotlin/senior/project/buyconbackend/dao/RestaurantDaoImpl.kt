package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Location
import senior.project.buyconbackend.entity.Menu
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.repository.RestaurantRepository

@Repository
class RestaurantDaoImpl: RestaurantDao {

    @Autowired
    lateinit var restaurantRepository: RestaurantRepository

    override fun getRestaurants(): List<Restaurant> {
        return restaurantRepository.findAll().toList()
    }

    override fun getRestaurantByName(name: String): Restaurant? {
        return restaurantRepository.findByName(name)
    }

    override fun save(restaurant: Restaurant): Restaurant {
        return restaurantRepository.save(restaurant)
    }

    override fun findById(id: Long): Restaurant? {
        return restaurantRepository.findById(id).orElse(null)
    }
}