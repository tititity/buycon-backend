package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Customer

interface CustomerDao {

    fun findById(custId: Long): Customer?
}