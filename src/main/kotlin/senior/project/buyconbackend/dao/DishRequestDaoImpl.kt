package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.DishRequest
import senior.project.buyconbackend.repository.DishRepository
import senior.project.buyconbackend.repository.DishRequestRepository
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Repository
class DishRequestDaoImpl: DishRequestDao {

    @Autowired
    lateinit var dishRequestRepository: DishRequestRepository

    @Autowired
    lateinit var dishRepository: DishRepository

    override fun getOrders(): List<DishRequest> {
        return dishRequestRepository.findAll().toList()
    }

    override fun save(dishRequest: DishRequest): DishRequest {
        dishRequest.dishes.forEach { dishRepository.save(it) }
        if(dishRequest.dateTime == null){
            var date = LocalDateTime.now()
            var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            var dateStr = date.format(formatter)
            dishRequest.dateTime = dateStr
        }
        return dishRequestRepository.save(dishRequest)
    }

    override fun findByCustId(custId: Long): List<DishRequest> {
        return dishRequestRepository.findByCustomer_Id(custId)
    }
    override fun findByRestId(restId: Long): List<DishRequest> {
        return dishRequestRepository.findByRestaurant_Id(restId)
    }

    override fun findById(id: Long): DishRequest {
        return dishRequestRepository.findById(id).orElse(null)
    }
}