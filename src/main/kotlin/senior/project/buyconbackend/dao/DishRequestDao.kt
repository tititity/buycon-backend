package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.DishRequest

interface DishRequestDao {

    fun getOrders(): List<DishRequest>
    fun save(dishRequest: DishRequest): DishRequest
    fun findByCustId(custId: Long): List<DishRequest>
    fun findByRestId(restId: Long): List<DishRequest>
    fun findById(id: Long): DishRequest
}