package senior.project.buyconbackend.dao

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import senior.project.buyconbackend.entity.Location
import senior.project.buyconbackend.repository.LocationRepository

@Repository
class LocationDaoImpl: LocationDao {

    @Autowired
    lateinit var locationRepository: LocationRepository
    override fun save(location: Location): Location {
        return locationRepository.save(location)
    }
}