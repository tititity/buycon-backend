package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Location

interface LocationDao {
    fun save(location: Location): Location
}