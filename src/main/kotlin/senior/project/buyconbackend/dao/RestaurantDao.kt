package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Restaurant

interface RestaurantDao {
    fun getRestaurants(): List<Restaurant>
    fun getRestaurantByName(name: String): Restaurant?
    fun save(restaurant: Restaurant): Restaurant
    fun findById(id: Long): Restaurant?
}