package senior.project.buyconbackend.dao

import senior.project.buyconbackend.entity.Dish

interface DishDao {

    fun getDishByRestName(name: String): List<Dish>
    fun save(dish: Dish): Dish
    fun getDishById(id: Long): Dish

}