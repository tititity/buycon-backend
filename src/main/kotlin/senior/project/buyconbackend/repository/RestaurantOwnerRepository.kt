package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.RestaurantOwner

interface RestaurantOwnerRepository: CrudRepository<RestaurantOwner, Long> {
}