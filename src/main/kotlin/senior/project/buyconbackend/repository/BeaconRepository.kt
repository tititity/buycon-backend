package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.Beacon

interface BeaconRepository: CrudRepository<Beacon,Long> {
    fun findByRestaurant_Id(restId: Long): List<Beacon>
    fun findByRestaurant_Name(restName: String): List<Beacon>
}