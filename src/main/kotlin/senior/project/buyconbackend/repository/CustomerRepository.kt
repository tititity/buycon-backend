package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.Customer

interface CustomerRepository: CrudRepository<Customer, Long> {
}