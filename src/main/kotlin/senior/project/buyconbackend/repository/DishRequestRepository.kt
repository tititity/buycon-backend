package senior.project.buyconbackend.repository

import org.springframework.data.repository.CrudRepository
import senior.project.buyconbackend.entity.DishRequest

interface DishRequestRepository: CrudRepository<DishRequest, Long> {
    fun findByCustomer_Id(custId: Long): List<DishRequest>
    fun findByRestaurant_Id(restId:Long): List<DishRequest>
}