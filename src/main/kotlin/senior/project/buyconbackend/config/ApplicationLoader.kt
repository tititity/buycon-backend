package senior.project.buyconbackend.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import senior.project.buyconbackend.entity.*
import senior.project.buyconbackend.repository.*
import java.time.LocalDateTime
import java.util.*
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner {
    @Value("\${spring.profiles.active}")
    val profileName: String? = null
    @Autowired
    lateinit var restaurantRepository: RestaurantRepository

    @Autowired
    lateinit var dishRepository: DishRepository

    @Autowired
    lateinit var locationRepository: LocationRepository

    @Autowired
    lateinit var beaconRepository: BeaconRepository

    @Autowired
    lateinit var restaurantOwnerRepository: RestaurantOwnerRepository

    @Autowired
    lateinit var customerRepository: CustomerRepository

    @Autowired
    lateinit var dishRequestRepository: DishRequestRepository



    @Transactional
    override fun run(args: ApplicationArguments?){


//        if (profileName.equals("heroku")) {
//            return
//        }

        var lo1 = locationRepository.save(Location("13.75398", "100.50144"))

        var res1 = restaurantRepository.save(Restaurant("Thai smile", "tan's restaurant", "https://ibb.co/8K7ktkv"))

        var dish1 = dishRepository.save(Dish("Pad thai",45.00,"https://img.kapook.com/u/2015/surauch/cook2/PT1.jpg","This is Pad thai"))
        var dish2 = dishRepository.save(Dish("Som tum", 30.00,"http://www.kroobank.com/15870/project/images/slide/obf7q4vcoae8tSdhqK3-o.jpg","This is Som tum"))

        var beacon1 = beaconRepository.save(Beacon("myBeacon","FDA50693-A4E2-4FB1-AFCF-C6EB07647825",19641))

        res1.location = lo1

        res1.dishes.add(dish1)
        dish1.restaurant = res1

        res1.dishes.add(dish2)
        dish2.restaurant = res1

        res1.beacons.add(beacon1)
        beacon1.restaurant = res1

        var lo2 = locationRepository.save(Location("18.796224", "99.005377"))

        var res2 = restaurantRepository.save(Restaurant("Vietnam smile", "Vietnam food restaurant", "https://phuket.weserve.co.th/media/upload/store/04022017151737.jpg"))

        var dish3 = dishRepository.save(Dish("Namnueng", 150.0, "http://img.painaidii.com/images/20111120_3_1321775396_292105.jpg", "This is Namnueng"))
        var dish4 = dishRepository.save(Dish("Fried Shrimp with Sugar Cane", 150.0, "https://pbs.twimg.com/media/D29SCsMWsAAfVJ8.jpg", "This is Fried Shrimp with Sugar Cane"))
        var dish5 = dishRepository.save(Dish("Vietnamese rolls", 75.0, "https://tastesbetterfromscratch.com/wp-content/uploads/2013/03/Fresh-Spring-Rolls-15.jpg", "This is Vietnam rolls"))

        var beacon2 = beaconRepository.save(Beacon("VTBeacon", "a36a12e9-2a72-4154-b2af-03a788867a0f", 65533))

        res2.location = lo2

        res2.dishes.add(dish3)
        res2.dishes.add(dish4)
        res2.dishes.add(dish5)

        dish3.restaurant = res2
        dish4.restaurant = res2
        dish5.restaurant = res2

        res2.beacons.add(beacon2)
        beacon2.restaurant = res2

        var owner1 = restaurantOwnerRepository.save(RestaurantOwner("PTan","tan@email.com","1234"))

        owner1.restaurant = res1

        var owner2 = restaurantOwnerRepository.save(RestaurantOwner("Hong","hong@email.com","5022"))

        owner2.restaurant = res2

        var customer1 = customerRepository.save(Customer("titi","titi@email.com","5032"))
        var customer2 = customerRepository.save(Customer("sun", "sun@gmail.com","5023"))
        var customer3 = customerRepository.save(Customer("touch", "touch@gmail.com", "5017"))

//         RESTAURANT 3
        var owner3 = restaurantOwnerRepository.save(RestaurantOwner("Bas", "bas@gamail.com", "5033"))

        var lo3 = locationRepository.save(Location("18.792586", "98.956148"))

        var rest3 = restaurantRepository.save(Restaurant("Changphuak Suki", "Best suki in Thailand. Don't miss. Local price but tasty. Vegetables are fresh!", "https://img.wongnai.com/p/1920x0/2019/09/09/731b7aae94ec4c1dab8710f96f66c30c.jpg"))

        var dish6 = dishRepository.save(Dish("Noodle with porks", 35.00, "https://img.wongnai.com/p/1920x0/2018/05/01/3ab5f24c8adc42d5b38fad232589a85d.jpg", "Noodle with proks"))
        var dish7 = dishRepository.save(Dish("Suki Moo", 50.00, "https://img.wongnai.com/p/1920x0/2018/05/05/0a383fbeacca4635832ec9d5969c6aad.jpg", "Suki yaki with porks"))
        var dish8 = dishRepository.save(Dish("Suki Nuea", 60.00, "https://img.wongnai.com/p/1920x0/2018/05/05/8b468fbb9ab546a0ab80d0c964bbeb8f.jpg"))
        var dish9 = dishRepository.save(Dish("Suki Ta Lay", 70.00, "https://img.wongnai.com/p/1920x0/2019/09/09/66d4cac8dc6e46c1b8cd7d5059c6d595.jpg" ))

        var beacon3 = beaconRepository.save(Beacon("Changphuak Suki", "", 12345))

        rest3.beacons.add(beacon3)
        rest3.location = lo3

        rest3.dishes.add(dish6)
        rest3.dishes.add(dish7)
        rest3.dishes.add(dish8)
        rest3.dishes.add(dish9)

        dish6.restaurant = rest3
        dish7.restaurant = rest3
        dish8.restaurant = rest3
        dish9.restaurant = rest3

        owner3.restaurant = rest3


        var req1 = dishRequestRepository.save(DishRequest("3", false, "2019-10-22"))
        var req2 = dishRequestRepository.save(DishRequest("5", false, "2019-10-27"))

        req1.customer = customer1
        req1.restaurant = res1
        req1.dishes.add(dish1)

        req2.customer = customer1
        req2.restaurant = res1
        req2.dishes.add(dish3)
        req2.dishes.add(dish5)

//        RESTAURANT 4
        var lo4 = locationRepository.save(Location("18.795262", "98.967706"))
        var res4 = restaurantRepository.save(Restaurant("Beast Burger", "The best burger in Chiang mai", "https://img.wongnai.com/p/1920x0/2019/06/03/3a55027e77d6484eb7008e8014917048.jpg"))

        res4.location = lo4

        var dish10 = dishRepository.save(Dish("The beast", 250.00, "https://img.wongnai.com/p/1920x0/2016/05/22/427de92ca6b54fc79c9873fd74bc2484.jpg"))
        var dish11 = dishRepository.save(Dish("Double double", 200.00, "https://img.wongnai.com/p/1920x0/2017/04/16/2c61c3ae6481412ab4554784b6462f8c.jpg"))
        var dish12 = dishRepository.save(Dish("Creamy blue cheese", 190.00, "https://img.wongnai.com/p/1920x0/2019/05/30/7c39624397694f77a8407f943c71658a.jpg"))
        var dish13 = dishRepository.save(Dish("Waffle fries", 100.00, "https://img.wongnai.com/p/1920x0/2016/06/29/27316dc25ce741669bee8ed76e788857.jpg"))

        var beacon4 = beaconRepository.save(Beacon("Beast Burger beacon", "00001803-494c04f47-4943-544543800000",2))

        res4.dishes.add(dish10)
        res4.dishes.add(dish11)
        res4.dishes.add(dish12)
        res4.dishes.add(dish13)

        res4.beacons.add(beacon4)



    }
}