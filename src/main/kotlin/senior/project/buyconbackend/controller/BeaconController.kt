package senior.project.buyconbackend.controller

import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import senior.project.buyconbackend.entity.dto.BeaconDto
import senior.project.buyconbackend.service.BeaconService
import senior.project.buyconbackend.util.MapperUtil


@RestController
class BeaconController {

    @Autowired
    lateinit var beaconService: BeaconService

    @GetMapping("/beacons/{restId}")
    fun getBeaconByRestId(@PathVariable("restId") restId: Long): ResponseEntity<Any>{
        val beacons = beaconService.getBeaconByRestId(restId)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBeaconDto(beacons))
    }

    @PostMapping("/add/beacon/{restId}")
    fun addBeacon(@RequestBody beaconDto: BeaconDto, @PathVariable("restId") restId: Long): ResponseEntity<Any>{
        val output = beaconService.save(MapperUtil.INSTANCE.mapBeaconDto(beaconDto), restId)
        val outputDto = MapperUtil.INSTANCE.mapBeaconDto(output)
        return ResponseEntity.ok(outputDto)
    }

    @GetMapping("/beacons/restName/{restName}")
    fun getBeaconByRestName(@PathVariable("restName")restName: String): ResponseEntity<Any>{
        val beacons = beaconService.getBeaconByRestName(restName)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapBeaconDto(beacons))
    }
}