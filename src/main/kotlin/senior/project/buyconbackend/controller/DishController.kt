package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import senior.project.buyconbackend.entity.dto.DishDto
import senior.project.buyconbackend.service.DishService
import senior.project.buyconbackend.util.MapperUtil

@RestController
class DishController {

    @Autowired
    lateinit var dishService: DishService

    @GetMapping("/dish/{restaurant}")
    fun getDishByRestName(@PathVariable("restaurant") name: String): ResponseEntity<Any>{
        val dishes = dishService.getDishByRestName(name)
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapDishDto(dishes))
    }

//    add dish without restaurant id/name
    @PostMapping("/add/dish")
    fun addDish(@RequestBody dishDto: DishDto) : ResponseEntity<Any>{

        val output = dishService.save(MapperUtil.INSTANCE.mapDishDto(dishDto))
        val outputDto = MapperUtil.INSTANCE.mapDishDto(output)
        outputDto?.let { return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    //add dish with restaurant id
    @PostMapping("/dish/restaurant/{restId}")
    fun addDish(@PathVariable("restId") restId: Long, @RequestBody dishDto: DishDto): ResponseEntity<Any>{
        val output = dishService.save(restId, MapperUtil.INSTANCE.mapDishDto(dishDto))
        val outputDto = MapperUtil.INSTANCE.mapDishDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    //add a list of dishes with restaurant id
    @PostMapping("/add/dishes/{restId}")
    fun addDishes(@PathVariable("restId") restId: Long, @RequestBody dishDto: List<DishDto>): ResponseEntity<Any>{
        val output = dishDto.map{
            it -> dishService.save(restId, MapperUtil.INSTANCE.mapDishDto(it))

        }
        val outputDto = MapperUtil.INSTANCE.mapDishDto(output)
        outputDto?.let{ return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }
}