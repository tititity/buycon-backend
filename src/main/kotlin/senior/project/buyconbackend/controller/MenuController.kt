package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import senior.project.buyconbackend.service.MenuService

@RestController
class MenuController {
    @Autowired
    lateinit var menuService: MenuService

    @GetMapping("menus")
    fun getMenus(): ResponseEntity<Any>{
        return ResponseEntity.ok(menuService.getMenus())
    }
}