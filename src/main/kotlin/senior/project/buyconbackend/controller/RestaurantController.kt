package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.EmptyResultDataAccessException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import senior.project.buyconbackend.entity.Restaurant
import senior.project.buyconbackend.entity.dto.RestaurantDto
import senior.project.buyconbackend.service.RestaurantService
import senior.project.buyconbackend.util.MapperUtil

@RestController
class RestaurantController {

    @Autowired
    lateinit var restaurantService: RestaurantService

    @GetMapping("/restaurants")
    fun getRestaurants(): ResponseEntity<Any> {
        val restaurants = restaurantService.getRestaurants()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapRestaurantDto(restaurants))
    }

    @GetMapping("/restaurant/{name}")
    fun getRestaurantByName(@PathVariable("name") name: String): ResponseEntity<Any>{

        //handle Result must not be null (EmptyResultDataAccessException)
        try {
            val output = MapperUtil.INSTANCE.mapRestaurantDto(restaurantService.getRestaurantByName(name))
            output?.let { return ResponseEntity.ok(it) }
        } catch (e: EmptyResultDataAccessException){
            return ResponseEntity.ok("not found")
        }
        return ResponseEntity.ok("end")
    }

    @PostMapping("/add/restaurant")
    fun addRestaurant(@RequestBody restDto: RestaurantDto): ResponseEntity<Any>{

        return ResponseEntity.ok(MapperUtil.INSTANCE.mapRestaurant(restaurantService.save(restDto)))
    }
}