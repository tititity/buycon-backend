package senior.project.buyconbackend.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import senior.project.buyconbackend.entity.dto.DishRequestDto
import senior.project.buyconbackend.service.DishRequestService
import senior.project.buyconbackend.util.MapperUtil


@RestController
class DishRequestController {

    @Autowired
    lateinit var dishRequestService: DishRequestService

    //get all orders
    @GetMapping("/orders")
    fun getOrders(): ResponseEntity<Any>{
        val orders = dishRequestService.getOrders()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapDishRequest(orders))
    }

    //add order
    @PostMapping("/add/order/{restId}/{custId}")
    fun addOrder(@RequestBody dishRequestDto: DishRequestDto, @PathVariable("restId") restId: Long, @PathVariable("custId") custId: Long): ResponseEntity<Any>{
        val order = dishRequestService.save(restId, custId, dishRequestDto)
        val output = MapperUtil.INSTANCE.mapDishRequest(order)
        output?.let{ return ResponseEntity.ok(it)}
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    //customer get order
    @GetMapping("/orders/customer/{custId}")
    fun getOrderByCustId(@PathVariable("custId") custId: Long): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapDishRequest(dishRequestService.getOrderByCustId(custId))
        return ResponseEntity.ok(output)
    }

    //restaurant get order
    @GetMapping("orders/restaurant/{restId}")
    fun getOrderByRestId(@PathVariable("restId") restId: Long): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapDishRequest(dishRequestService.getOrderByRestId(restId))
        return ResponseEntity.ok(output)

    }

    //confirm order
    @PutMapping("/confirm/{restId}/{custId}")
    fun confirmOrder(@RequestBody orderDto: DishRequestDto, @PathVariable("restId") restId: Long, @PathVariable("custId") custId: Long): ResponseEntity<Any>{
        if(orderDto.id !== null && orderDto.isConfirm !== null){
            val output = MapperUtil.INSTANCE.mapDishRequest(dishRequestService.save(restId,custId,orderDto))
            return ResponseEntity.ok(output)
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("the order must not be null")
    }

    @GetMapping("/order/{orderId}")
    fun getOrder(@PathVariable("orderId") orderId: Long): ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapDishRequest(dishRequestService.getOrderById(orderId))
        return ResponseEntity.ok(output)
    }
}