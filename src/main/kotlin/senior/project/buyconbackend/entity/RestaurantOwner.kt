package senior.project.buyconbackend.entity

import javax.persistence.*

@Entity
data class RestaurantOwner(
        var username: String,
        var email: String,
        var password: String
) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var restaurant: Restaurant? =null


}