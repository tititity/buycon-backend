package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Location(var latitude: String? = null,
                    var longitude: String? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null
}