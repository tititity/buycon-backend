package senior.project.buyconbackend.entity

import javax.persistence.*

@Entity
data class Restaurant(var name: String? = null,
                      var description: String? = null,
                      var image: String? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var location: Location? = null

    @OneToMany
    var dishes = mutableListOf<Dish>()

    @OneToMany
    var beacons = mutableListOf<Beacon>()



    constructor(name: String,
                description: String,
                image: String,
                location: Location): this(name, description, image){
        this.location = location
    }

}