package senior.project.buyconbackend.entity

import java.time.LocalDateTime
import javax.persistence.*

@Entity
data class DishRequest(var tbNo : String? = null,
                       var isConfirm: Boolean? = false,
                       var dateTime: String? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var dishes = mutableListOf<Dish>()

    @ManyToOne(cascade = [CascadeType.ALL])
    var restaurant: Restaurant? = null

    @ManyToOne(cascade = [CascadeType.ALL])
    var customer: Customer? = null

    constructor(tbNo: String,
                dishes: List<Dish>,
                restaurant: Restaurant,
                customer: Customer,
                isConfirm: Boolean):this(tbNo, isConfirm){
        this.dishes.addAll(dishes)
        this.restaurant = restaurant
        this.customer = customer
    }
}