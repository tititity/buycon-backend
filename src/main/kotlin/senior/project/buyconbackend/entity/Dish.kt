package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Dish(var dishName: String? = null,
                var price: Double? = null,
                var image: String? = null,
                var dishDesc: String? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var restaurant: Restaurant? = null

    constructor(dishName: String,
                price: Double,
                image: String,
                dishDesc: String,
                restuarant: Restaurant): this(dishName, price, image, dishDesc){
        this.restaurant = restaurant
    }

}