package senior.project.buyconbackend.entity

import java.awt.Image

data class Menu(var name: String) {
    var dishes = mutableListOf<Dish>()
}