package senior.project.buyconbackend.entity.dto

import senior.project.buyconbackend.entity.Location

data class RestaurantDto(
        var id: Long? = null,
        var name: String? = null,
        var description: String? = null,
        var image: String? = null,
        var location: Location? = null,
        var dishes: List<DishDto>? = emptyList(),
        var beacons: List<BeaconDto>? = emptyList()) {
}