package senior.project.buyconbackend.entity.dto

data class BeaconDto(var id: Long? = null,
                     var beaconName: String? = null,
                     var UUID: String? = null,
                     var minor: Int? = null) {
}