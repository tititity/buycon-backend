package senior.project.buyconbackend.entity.dto

import senior.project.buyconbackend.entity.Customer
import java.time.LocalDateTime

data class DishRequestDto(var id: Long? = null,
                          var tbNo: String? = null,
                          var dishes: List<DishDto>? = emptyList(),
                          var customer: Customer? = null,
                          var restaurant: RestaurantDto? = null,
                          var dateTime: String? = null,
                          var isConfirm: Boolean? = false) {
}