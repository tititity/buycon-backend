package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class Customer(
        var username: String,
        var email: String,
        var password: String
) {

    @Id
    @GeneratedValue
    var id: Long? = null



}