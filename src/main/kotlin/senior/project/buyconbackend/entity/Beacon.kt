package senior.project.buyconbackend.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Beacon(var beaconName: String? = null,
                  var UUID: String? = null,
                  var minor: Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var restaurant: Restaurant? = null
}