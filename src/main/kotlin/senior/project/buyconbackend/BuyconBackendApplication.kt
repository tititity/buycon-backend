package senior.project.buyconbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BuyconBackendApplication

fun main(args: Array<String>) {
    runApplication<BuyconBackendApplication>(*args)
}
